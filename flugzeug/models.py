from django.db import models

class Flugzeug(models.Model):
    name = models.CharField(help_text='Airbus A330', max_length=50)
    maxpersonen = models.IntegerField()

    fluggesellschaft = models.ForeignKey('fluggesellschaft.Fluggesellschaft', on_delete=models.CASCADE)



    def __str__(self):
        return self.name