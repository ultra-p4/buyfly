from django.shortcuts import render
from rest_framework import viewsets

from flugzeug.models import Flugzeug
from flugzeug.serializers import FlugzeugSerializer, FlugzeugDetailSerializer


class FlugzeugView(viewsets.ModelViewSet):
    serializer_class = FlugzeugSerializer
    queryset = Flugzeug.objects.all()

    def get_serializer_class(self):
        if self.detail:
            return FlugzeugDetailSerializer
        return FlugzeugSerializer