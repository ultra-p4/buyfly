from rest_framework.routers import DefaultRouter

from flugzeug.views import FlugzeugView

router = DefaultRouter()
router.register(r'flugzeug', FlugzeugView, base_name='flugzeug')

urlpatterns = router.urls