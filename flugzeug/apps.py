from django.apps import AppConfig


class FlugzeugConfig(AppConfig):
    name = 'flugzeug'
