from rest_framework import serializers

from flugzeug.models import Flugzeug


class FlugzeugSerializer(serializers.ModelSerializer):
    class Meta:
        model = Flugzeug
        fields = ('id', 'name', )


class FlugzeugDetailSerializer(serializers.ModelSerializer):
    class Meta:
        model = Flugzeug
        fields = ('id', 'name', 'maxpersonen', )


