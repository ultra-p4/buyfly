from django.contrib import admin

from flugzeug.models import Flugzeug


@admin.register(Flugzeug)
class AdminFlugzeug(admin.ModelAdmin):
    list_display = ('id', 'name', 'maxpersonen', )
    search_fields = ('id', 'name', )