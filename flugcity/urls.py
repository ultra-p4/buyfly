from rest_framework.routers import DefaultRouter

from flugcity.views import FlugcityView

router = DefaultRouter()
router.register(r'flugcity', FlugcityView, base_name='flugcity')

urlpatterns = router.urls