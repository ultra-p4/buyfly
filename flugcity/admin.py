from django.contrib import admin

from flugcity.models import Flugcity


@admin.register(Flugcity)
class AdminFlugcity(admin.ModelAdmin):
    list_display=('id', 'abflugdestination', 'zieldestination', 'flug_preis', 'city_ort', 'flug_id', )
    search_fields = ('id', )

#damit flugpreis in flugcity angezeigt wird
    def flug_preis(self, obj):
        return obj.flug.preis


#damit city ort in flugcity angezeigt wird
    def city_ort(self, obj):
        return obj.city.ort
