from rest_framework import serializers

from city.serializers import CitySerializer
from flugcity.models import Flugcity


class FlugcitySerializer(serializers.ModelSerializer):
    class Meta:
        model = Flugcity
        fields = ('id', 'abflugdestination', 'zieldestination',)


class FlugcityDetailSerializer(serializers.ModelSerializer):
    # city = CitySerializer()
    ort = serializers.ReadOnlyField(source='city.ort')

    class Meta:
        model = Flugcity
        fields = ('id', 'abflugdestination', 'zieldestination', 'ort',)
