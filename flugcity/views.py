from django.shortcuts import render
from rest_framework import viewsets

from flugcity.models import Flugcity
from flugcity.serializers import FlugcitySerializer, FlugcityDetailSerializer


class FlugcityView(viewsets.ModelViewSet):
    serializer_class = FlugcitySerializer
    queryset = Flugcity.objects.all()

    def get_serializer_class(self):
        if self.detail:
            return FlugcityDetailSerializer
        return FlugcitySerializer