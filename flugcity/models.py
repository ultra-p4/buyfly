from django.db import models


class Flugcity(models.Model):
    abflugdestination = models.BooleanField()
    zieldestination = models.BooleanField()

    flug = models.ForeignKey('flug.Flug', on_delete=models.CASCADE)
    city = models.ForeignKey('city.City', on_delete=models.CASCADE)

    def __str__(self):
        return '%s' % (self.city.ort,)

    def __str__(self):
        return '%s' % (self.flug.preis,)

    def __str__(self):
        return '%s' % (self.flug.id,)
