from django.apps import AppConfig


class FlugcityConfig(AppConfig):
    name = 'flugcity'
