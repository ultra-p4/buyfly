from django.contrib import admin

from city.models import City


@admin.register(City)
class AdminCity(admin.ModelAdmin):
    list_display=('ort', 'airport', 'airportcode', )
    search_fields = ('ort', )
