from django.shortcuts import render
from rest_framework import viewsets

from city.models import City
from city.serializers import CitySerializer, CityDetailSerializer


class CityView(viewsets.ModelViewSet):
    serializer_class = CitySerializer
    queryset = City.objects.all()

    def get_serializer_class(self):
        if self.detail:
            return CityDetailSerializer
        return CitySerializer