from rest_framework import serializers

from city.models import City


class CitySerializer(serializers.ModelSerializer):
    class Meta:
        model = City
        fields = ('id', 'ort', 'airport', 'airportcode', )


class CityDetailSerializer(serializers.ModelSerializer):
    class Meta:
        model = City
        fields = ('id', 'ort', 'airport', 'airportcode', )