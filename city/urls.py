from rest_framework.routers import DefaultRouter

from city.views import CityView

router = DefaultRouter()
router.register(r'city', CityView, base_name='city')

urlpatterns = router.urls