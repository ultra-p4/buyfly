from django.db import models


class City(models.Model):
    ort = models.CharField(help_text='London', max_length=50)
    airport = models.CharField(help_text='Gatewick', max_length=50)
    airportcode = models.CharField(help_text='LGW', max_length=20)


    def __str__(self):
        return '%s' % self.ort

