from rest_framework import serializers

from fluggesellschaft.models import Fluggesellschaft


class FluggesellschaftSerializer(serializers.ModelSerializer):
    class Meta:
        model = Fluggesellschaft
        fields = ('id', 'name', )


class FluggesellschaftDetailSerializer(serializers.ModelSerializer):
    class Meta:
        model = Fluggesellschaft
        fields = ('id', 'name', )