from django.shortcuts import render
from rest_framework import viewsets

from fluggesellschaft.models import Fluggesellschaft
from fluggesellschaft.serializers import FluggesellschaftSerializer, FluggesellschaftDetailSerializer


class FluggesellschaftView(viewsets.ModelViewSet):
    serializer_class = FluggesellschaftSerializer
    queryset = Fluggesellschaft.objects.all()

    def get_serializer_class(self):
        if self.detail:
            return FluggesellschaftDetailSerializer
        return FluggesellschaftSerializer