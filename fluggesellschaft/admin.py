from django.contrib import admin

from fluggesellschaft.models import Fluggesellschaft


@admin.register(Fluggesellschaft)
class AdminFluggesellschaft(admin.ModelAdmin):
    list_display = ('id', 'name', )
    search_fields = ('name', )
