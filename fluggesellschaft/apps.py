from django.apps import AppConfig


class FluggesellschaftConfig(AppConfig):
    name = 'fluggesellschaft'
