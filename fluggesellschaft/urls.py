from rest_framework.routers import DefaultRouter

from fluggesellschaft.views import FluggesellschaftView

router = DefaultRouter()
router.register(r'fluggesellschaft', FluggesellschaftView, base_name='fluggesellschaft')

urlpatterns = router.urls