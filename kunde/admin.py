from django.contrib import admin

from kunde.models import Kunde



@admin.register(Kunde)
class AdminKunde(admin.ModelAdmin):
    list_display=('id', 'vorname', 'nachname', 'gepaeck', 'strasse', 'hausnummer', 'plz', 'ort', 'buchungen', )
    search_fields = ('vorname', 'nachname', )


