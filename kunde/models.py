from django.db import models
from flug.models import Flug

class Kunde(models.Model):
    vorname = models.CharField(help_text='Hans', max_length=50)
    nachname = models.CharField(help_text='Meier', max_length=50)
    strasse = models.CharField(help_text='Luzernerstrasse', max_length=50)
    hausnummer = models.IntegerField()
    plz = models.IntegerField()
    ort = models.CharField(help_text='Luzern', max_length=50)
    gepaeck = models.BooleanField()
    flug = models.ManyToManyField('flug.Flug',)


    #Buttonbeschriftung
    def __str__(self):
        return self.vorname

#liste aller Flug Buchungen vom Kunden

    @property
    def buchungen(self):
        buchungen=', '.join( [str(buchung.id) for buchung in self.flug.all()] )
        print (buchungen)
        return buchungen