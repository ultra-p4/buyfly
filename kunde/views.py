from django.shortcuts import render
from rest_framework import viewsets

from kunde.models import Kunde
from kunde.serializers import KundeSerializer, KundeDetailSerializer


class KundeView(viewsets.ModelViewSet):
    serializer_class = KundeSerializer
    queryset = Kunde.objects.all()

    def get_serializer_class(self):
        if self.detail:
            return KundeDetailSerializer
        return KundeSerializer