# Generated by Django 2.0.5 on 2018-05-23 13:58

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('flug', '0005_auto_20180523_1553'),
        ('kunde', '0003_auto_20180523_1313'),
    ]

    operations = [
        migrations.AddField(
            model_name='kunde',
            name='flug',
            field=models.ManyToManyField(to='flug.Flug'),
        ),
    ]
