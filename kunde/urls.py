from rest_framework.routers import DefaultRouter

from kunde.views import KundeView

router = DefaultRouter()
router.register(r'kunde', KundeView, base_name='kunde')


urlpatterns = router.urls