from rest_framework import serializers

from kunde.models import Kunde


class KundeSerializer(serializers.ModelSerializer):
    class Meta:
        model = Kunde
        fields = ('id', 'vorname', 'nachname', 'strasse', 'hausnummer', 'plz', 'ort', 'gepaeck', 'flug', )


class KundeDetailSerializer(serializers.ModelSerializer):
    class Meta:
        model = Kunde
        fields = ('id', 'vorname', 'nachname', 'strasse', 'hausnummer', 'plz', 'ort', 'gepaeck', 'flug', )