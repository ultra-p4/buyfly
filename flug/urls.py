from rest_framework.routers import DefaultRouter

from flug.views import FlugView

router = DefaultRouter()
router.register(r'flug', FlugView, base_name='flug')

urlpatterns = router.urls