from django.contrib import admin

from flug.models import Flug


@admin.register(Flug)
class AdminFlug(admin.ModelAdmin):
    list_display=('id', 'storniert', 'abflugzeit', 'abflugdatum', 'landezeit', 'landedatum', 'preis', 'flugzeug', )
    search_fields = ('id', )

