from django.db import models


class Flug(models.Model):
    abflugzeit = models.TimeField()
    abflugdatum = models.DateField()
    landezeit = models.TimeField()
    landedatum = models.DateField()
    preis = models.IntegerField(help_text='in CHF', )

    flugzeug = models.ForeignKey('flugzeug.Flugzeug', on_delete=models.CASCADE)


    storniert = models.BooleanField()


    def __str__(self):
        return '%s' % self.id
