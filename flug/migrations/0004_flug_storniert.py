# Generated by Django 2.0.5 on 2018-05-23 11:35

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('flug', '0003_auto_20180523_1313'),
    ]

    operations = [
        migrations.AddField(
            model_name='flug',
            name='storniert',
            field=models.BooleanField(default=False),
            preserve_default=False,
        ),
    ]
