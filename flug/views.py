from django.shortcuts import render
from rest_framework import viewsets

from flug.models import Flug
from flug.serializers import FlugSerializer, FlugDetailSerializer


class FlugView(viewsets.ModelViewSet):
    serializer_class = FlugSerializer
    queryset = Flug.objects.all()

    def get_serializer_class(self):
        if self.detail:
            return FlugDetailSerializer
        return FlugSerializer