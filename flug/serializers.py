from rest_framework import serializers

from flug.models import Flug
from flugcity.models import Flugcity
from flugcity.serializers import FlugcityDetailSerializer
from flugzeug.serializers import FlugzeugSerializer


class FlugSerializer(serializers.ModelSerializer):
    class Meta:
        model = Flug
        fields = ('id', 'abflugzeit', 'abflugdatum', 'preis',)


class FlugDetailSerializer(serializers.ModelSerializer):
    flugzeug = FlugzeugSerializer()
    city = serializers.SerializerMethodField()

    class Meta:
        model = Flug
        fields = ('id', 'abflugzeit', 'preis', 'abflugdatum', 'landezeit', 'landedatum', 'flugzeug', 'city',)

    def get_city(self, obj):
        flugcity = Flugcity.objects.filter(flug=obj)
        return [FlugcityDetailSerializer(fc).data for fc in flugcity]
